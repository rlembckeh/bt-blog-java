package com.rlembcke.btblog.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rlembcke.btblog.model.Post;
import com.rlembcke.btblog.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.client.MockRestServiceServer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@ExtendWith(SpringExtension.class)
@RestClientTest(BlogPostService.class)
class BlogPostServiceTest {

    @Autowired
    private BlogPostService blogPostService;

    @Autowired
    private MockRestServiceServer server;

    @Autowired
    private ObjectMapper objectMapper;

    @BeforeEach
    public void setUp() throws Exception {
        String users = objectMapper.writeValueAsString(Arrays.asList(
                new User().setId(1).setName("User 1"),
                new User().setId(2).setName("User 2")
        ));
        this.server.expect(requestTo(BlogPostService.USERS_URI)).andRespond(withSuccess(users, MediaType.APPLICATION_JSON));

        String posts = objectMapper.writeValueAsString(Arrays.asList(
                new Post().setId(1).setUserId(1).setTitle("Post 1 for User 1"),
                new Post().setId(2).setUserId(1).setTitle("Post 2 for User 1"),
                new Post().setId(3).setUserId(2).setTitle("Post 1 for User 2")
        ));
        this.server.expect(requestTo(BlogPostService.POSTS_URI)).andRespond(withSuccess(posts, MediaType.APPLICATION_JSON));

    }


    @Test
    void getUsersAndPosts() {
        List<User> usersAndPosts = blogPostService.getUsersAndPosts();
        assertEquals(2, usersAndPosts.size());
        User user1 = usersAndPosts.stream().filter(user -> user.getId().equals(1)).findFirst().orElse(new User());
        assertEquals(2, user1.getPosts().size());

        User user2 = usersAndPosts.stream().filter(user -> user.getId().equals(2)).findFirst().orElse(new User());
        assertEquals(1, user2.getPosts().size());

    }
}