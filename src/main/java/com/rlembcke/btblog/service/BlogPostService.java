package com.rlembcke.btblog.service;

import com.rlembcke.btblog.model.Comment;
import com.rlembcke.btblog.model.Post;
import com.rlembcke.btblog.model.User;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BlogPostService {

    protected final static String USERS_URI = "https://jsonplaceholder.typicode.com/users";
    protected final static String POSTS_URI = "https://jsonplaceholder.typicode.com/posts";
    protected final static String COMMENTS_URI = "https://jsonplaceholder.typicode.com/comments?postId={postId}";

    private final RestTemplate restTemplate;

    public BlogPostService (RestTemplateBuilder restTemplateBuilder){
        restTemplate = restTemplateBuilder.build();
    }

    public List<User> getUsersAndPosts() throws HttpStatusCodeException {

        ResponseEntity<List<User>> responseUsers =
                restTemplate.exchange(USERS_URI,
                        HttpMethod.GET, null, new ParameterizedTypeReference<List<User>>() {
                        });

        ResponseEntity<List<Post>> responsePosts =
                restTemplate.exchange(POSTS_URI,
                        HttpMethod.GET, null, new ParameterizedTypeReference<List<Post>>() {
                        });


        List<User> users = responseUsers.getBody();
        List<Post> posts = responsePosts.getBody();

        if (!CollectionUtils.isEmpty(users)) {
            for (User user : users) {
                if (!CollectionUtils.isEmpty(posts)) {
                    user.setPosts(posts.stream().filter(post -> post.getUserId().equals(user.getId())).collect(Collectors.toList()));
                }

            }

        } else {
            return new ArrayList<>();
        }

        return users;

    }


    public List<Comment> getComments(@RequestParam Integer postId) throws HttpStatusCodeException{
        ResponseEntity<List<Comment>> response =
                restTemplate.exchange(COMMENTS_URI,
                        HttpMethod.GET, null, new ParameterizedTypeReference<List<Comment>>() {
                        }, postId);

        if (response.getBody() != null) {
            return response.getBody();
        } else {
            return new ArrayList<>();
        }
    }


}
