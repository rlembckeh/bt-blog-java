package com.rlembcke.btblog.controller;


import com.rlembcke.btblog.service.BlogPostService;
import org.slf4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpStatusCodeException;

import javax.annotation.Resource;

import static org.slf4j.LoggerFactory.getLogger;


@RestController
public class BlogPostController {

    private static final Logger logger = getLogger(BlogPostController.class);

    @Resource
    private BlogPostService blogPostService;

    @GetMapping("usersAndPosts")
    public ResponseEntity getUsersAndPosts() {

        try {
            return ResponseEntity.ok(blogPostService.getUsersAndPosts());

        } catch (HttpStatusCodeException e) {
            logger.error("BlogPostController [{},{}]", e.getResponseBodyAsString(), e);
            return ResponseEntity.status(e.getRawStatusCode()).headers(e.getResponseHeaders())
                    .body(null);
        }

    }


    @GetMapping("comments")
    public ResponseEntity getComments(@RequestParam Integer postId) {

        try {
            return ResponseEntity.ok(blogPostService.getComments(postId));

        } catch (HttpStatusCodeException e) {
            logger.error("BlogPostController [{},{}]", e.getResponseBodyAsString(), e);
            return ResponseEntity.status(e.getRawStatusCode()).headers(e.getResponseHeaders())
                    .body(null);
        }

    }

}
