# Java + UI Assignment for BT Panorama
This application was implemented using SpringBoot 2.2.5.RELEASE and Oracle Java 1.8 

## Running the application
mvn spring-boot:run

## API Documentation (Swagger)
http://localhost:8080/swagger-ui.html
